from django.db import models


class Topics(models.Model):
    """Тема которую изучает пользователь"""
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Topics"

    def __str__(self):
        """Возвращает строковое описание модели"""
        return self.text


class Entry(models.Model):
    """Информация изученная пользователем по теме"""
    topic = models.ForeignKey(Topics)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Entries"

    def __str__(self):
        """Возвращает строковое описание модели"""
        if len(self.text) > 50:
            return self.text[:50] + "..."
        else:
            return self.text


